"use strict;"

class Stopwatch {
  constructor() {
    this._time = null
  }

  start() {
    this._time = performance.now()
  }

  stop() {
    const time = performance.now() - this._time
    this._time = null
    return time
  }
}

const wait = ms => new Promise(resolve => setTimeout(resolve, ms));

window.jsPsychUtils = {}
window.jsPsychUtils.Stopwatch = Stopwatch
window.jsPsychUtils.wait = wait
