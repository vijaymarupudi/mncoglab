## Instructions

In this part of the study, you will estimate the result of multiplication expressions. 

The problems are meant to be difficult - we do not expect you to make a correct response, just try your best.

A expression will appear after the fixation point.  Once the screen says "write your best estimation" , you have up to 10 seconds to complete your answer.

Once you are finished entering your answer, hit [enter] to continue to the next question.
