const FACTORIAL_INSTRUCTIONS = `
<h2>Instructions</h2>
<p>In this part of the study, you will estimate the result of multiplication expressions.</p>
<p>The problems are meant to be difficult - we do not expect you to make a correct response, just try your best.</p>
<p>A expression will appear after the fixation point. Once the screen says “enter your best estimation” , you have up to 10 seconds to complete your answer.</p>
<p>Once you are finished entering your answer, hit [enter] to continue to the next question.</p>
`;

function makeConfigurations(first, second, calibration, calibrationOrder) {
  const items = {
    ASC: "1x2x3x4x5x6x7x8",
    DESC: "8x7x6x5x4x3x2x1",
    "6": {
      ASC: { stimulus: "1x2x3x4x5x6", answer: 720 },
      DESC: { stimulus: "6x5x4x3x2x1", answer: 720 }
    },
    "10": {
      ASC: { stimulus: "1x2x3x4x5x6x7x8x9x10", answer: 3628800 },
      DESC: { stimulus: "10x9x8x7x6x5x4x3x2x1", answer: 3628800 }
    }
  };

  return [
    items[first],
    items[second],
    items[calibration][calibrationOrder],
    items[first],
    items[second]
  ];
}

const STIMULI_CONFIGURATIONS = {
  "0": makeConfigurations("ASC", "DESC", 6, "ASC"),
  "1": makeConfigurations("ASC", "DESC", 10, "ASC"),
  "2": makeConfigurations("DESC", "ASC", 6, "DESC"),
  "3": makeConfigurations("DESC", "ASC", 10, "DESC")
};

console.log(STIMULI_CONFIGURATIONS);

const timeline = [];
let stimuli = null; // this is an array that will be initiated based on participant number.
let participantNumber = null;

// ## Participant number

timeline.push({
  timeline: [
    {
      type: "survey-text",
      questions: [
        {
          prompt: "Participant number",
          placeholder: "Number",
          required: true,
          name: "participantNumber"
        }
      ],
      show_clickable_nav: true,
      on_finish: data => {
        participantNumber = parseInt(
          JSON.parse(data.responses)["participantNumber"]
        );
      }
    }
  ],
  loop_function: () => {
    if (isNaN(participantNumber)) {
      return true;
    } // repeat if invalid participant number.
    stimuli = STIMULI_CONFIGURATIONS[participantNumber % 4];
    jsPsych.data.addProperties({ participantNumber: participantNumber });
    return false;
  }
});

// ## Instructions

timeline.push({
  type: "instructions",
  pages: [FACTORIAL_INSTRUCTIONS],
  show_clickable_nav: true
});

// ## Practice trial

timeline.push({
  type: "html-keyboard-response",
  stimulus: "Hit [spacebar] to continue to try a practice trial.",
  choices: [32] // space
});

timeline.push({
  type: "factorial-estimation",
  stimulus: "4x5x7x9x3x1x2"
});

// ## Actual Trial Intro

timeline.push({
  type: "html-keyboard-response",
  stimulus: `
    <p>We will continue to the experiment trials.</p>
    <p>Hit [spacebar] to start.</p>
  `,
  choices: [32] // space
});

// ## Actual Trials - First Half

for (let i = 0; i < 2; i++) {
  timeline.push({
    on_start: trial => {
      trial.stimulus = stimuli[i];
    },
    type: "factorial-estimation"
  });
}

// ## Calibration

timeline.push({
  type: "html-keyboard-response",
  choices: [32], // space,
  on_start: trial => {
    const problem = stimuli[2].stimulus;
    const answer = stimuli[2].answer;
    const stimulus = `<p>Great job. Here is the correct answer to another multiplication problem:</p>
      <p class="factorial-display">${problem} = ${answer}</p>
      <p>Press [spacebar] to continue.`;
    trial.stimulus = stimulus;
  }
});

// Actual Trials - Second Half

timeline.push({
  type: "html-keyboard-response",
  choices: [32], // space
  stimulus: `<p>In this next part of the study, you will estimate the result of more multiplication expressions.</p>

  <p>Once the screen says "enter your best estimation", you have up to 10 seconds to complete your answer.</p>

  <p>Once you are finished entering your answer, hit [enter] to continute to the next question.</p>

  <p>Hit [spacebar] to start.</p>
`
});

for (let i = 0; i < 2; i++) {
  timeline.push({
    on_start: trial => {
      trial.stimulus = stimuli[i + 3]; // + 2 done, + 1 calibration info
    },
    type: "factorial-estimation"
  });
}

timeline.push({
  type: "survey-text",
  on_start: trial => {
    trial.questions = [
      {
        prompt: `<p>In your first trial, you estimated {${stimuli[0]}}.</p>
<p>What was your strategy for estimating this multiplication expression?</p> `,
        required: true,
        placeholder: "",
        rows: 4,
        name: "firstTrialStrategy"
      }
    ];
  }
});

timeline.push({
  type: "survey-text",
  on_start: trial => {
    trial.questions = [
      {
        prompt: `<p>In your second trial, you estimated {${stimuli[1]}}.</p>
<p>What was your strategy for estimating this multiplication expression?</p> `,
        required: true,
        placeholder: "",
        rows: 4,
        name: "secondTrialStrategy"
      }
    ];
  }
});

timeline.push({
  type: "survey-text",
  on_start: trial => {
    trial.questions = [
      {
        prompt: `
<p>After you completed the first two trials, we provided the value of {${
          stimuli[2].stimulus
        }}. Then, we asked you to estimate {${stimuli[3]}} and {${
          stimuli[4]
        }} again.</p>

<p>Did knowing the value of {${
          stimuli[2].stimulus
        }} help you estimate the third and fourth trial?</p>

<p>Please describe how.</p>
        `,
        required: true,
        placeholder: "",
        rows: 4,
        name: "didCalibrationHelp"

      }
    ];
  }
});

timeline.push({
  type: "survey-html-form",
  on_start: trial => {
    trial.html = `
      <p>If you had a calculator, would you get the same answer for {${stimuli[0]}} and {${stimuli[1]}}?</p>

      <input type="radio" required name="sameAnswer" id="Yes" value="Yes"> <label for="Yes">Yes</label><br>
      <input type="radio" required name="sameAnswer" id="No" value="No"> <label for="No">No</label>

      <p>Please describe why</p>
      <div><textarea cols="80" rows="5" name="calculatorDescription" required></textarea></div>
    `
}});

timeline.push({
  type: "html-keyboard-response",
  choices: [32], // space
  stimulus: `You have completed the experiment. Press [spacebar] to see your data.`
});

jsPsych.init({
  timeline: timeline,
  experiment_width: 800,
  on_finish: () => jsPsych.data.displayData(),
  on_trial_finish: data => {
    if (localStorage.devMode) {
      console.log(data);
    }
  }
});
