"use strict";

jsPsych.plugins["factorial-estimation"] = (function() {
  var plugin = {};

  plugin.info = {
    name: "factorial-estimation",
    parameters: {}
  };

  plugin.trial = async function(displayElement, trial) {

    const settings = {
      stimulusDuration: 5000,
      countdownStartValue: 10,
      ...trial
    }

    displayElement.innerHTML = "";

    const factorialDisplay = document.createElement("p");
    factorialDisplay.innerText = "+";
    factorialDisplay.classList.add("factorial-display");
    displayElement.appendChild(factorialDisplay);

    // fixation time
    await jsPsychUtils.wait(1000);
    // show stimulus
    factorialDisplay.innerText = settings.stimulus
    await jsPsychUtils.wait(settings.stimulusDuration);

    factorialDisplay.innerHTML = `
      <p>You have <span id="factorial-countdown"></span> seconds to enter your best estimation.</p>
      <p>[enter] to continue.</p>
      <input id="factorial-input" pattern="[0-9]+"/>
    `

    const countdownSpan = factorialDisplay.querySelector('#factorial-countdown')
    const factorialInput = factorialDisplay.querySelector('#factorial-input')

    let inputCountdownValue = settings.countdownStartValue
    const stopwatch = new jsPsychUtils.Stopwatch()

    const updateCountdownSpan = () => countdownSpan.innerText = inputCountdownValue.toString()


    factorialInput.focus()
    updateCountdownSpan()
    stopwatch.start()

    setInterval(() => {
      inputCountdownValue -= 1
      updateCountdownSpan()
    }, 1000)

    const keyUpListener = e => {
      if (e.key === 'Enter') {
        const userValue = parseInt(factorialInput.value)
        // if integer, end the trial
        console.log(userValue)
        if (!isNaN(userValue)) {
          const rt = stopwatch.stop()
          factorialInput.removeEventListener('keyup', keyUpListener)
          jsPsych.finishTrial({ ...settings, estimation: userValue, rt: rt })
        }
      }
    }

    factorialInput.addEventListener('keyup', keyUpListener)

  };

  return plugin;
})();
